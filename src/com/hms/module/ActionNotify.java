        package com.hms.module;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.wowza.util.StringUtils;
import com.wowza.wms.application.IApplication;
import com.wowza.wms.application.IApplicationInstance;
import com.wowza.wms.client.IClient;
import com.wowza.wms.logging.WMSLogger;
import com.wowza.wms.logging.WMSLoggerFactory;
import com.wowza.wms.server.Server;
import com.wowza.wms.stream.IMediaStream;
import com.wowza.wms.stream.MediaStreamActionNotifyBase;
import com.wowza.wms.vhost.IVHost;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Hugo on 20/06/2016.
 */
public class ActionNotify extends MediaStreamActionNotifyBase {

    static WMSLogger logger = WMSLoggerFactory.getLogger(ActionNotify.class);
    static final String PUBLISH_EVENT = "publish";
    static final String PLAY_EVENT = "play";

    public void onPlay(IMediaStream mediaStream, String streamName, double var3, double var5, int var7) {

        if (mediaStream.isTranscodeResult()) {
            return;
        }
        notifyEvent(mediaStream, streamName, PLAY_EVENT);
    }

    @Override
    public void onPublish(IMediaStream mediaStream, String streamName, boolean isRecord, boolean isAppend) {
        if (mediaStream.isTranscodeResult()) {
            return;
        }
        notifyEvent(mediaStream, streamName, PUBLISH_EVENT);
    }

    private void notifyEvent(IMediaStream mediaStream, String streamName, String event) {
        String serverUrl = Server.getInstance().getProperties().getPropertyStr("server_url");
        if (StringUtils.isEmpty(serverUrl)) {
            logger.error("'server_url' property has not been set.");
            return;
        }
        IClient client = mediaStream.getClient();
        IApplication application = client.getApplication();
        IApplicationInstance  instance = client.getAppInstance();
        IVHost host = client.getVHost();

        Calendar rightNow = Calendar.getInstance();
        Date now = rightNow.getTime();
        DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-DD");
        DateFormat timeFormat = new SimpleDateFormat("hh:mm a");

        Map<String, String> params = new HashMap<>();
        if (client != null) {
            String referrer = client.getReferrer();
            String flashVer = client.getFlashVer();
            String pageUrl = client.getPageUrl();

            params.put("clientIp", client.getIp());
            if (!StringUtils.isEmpty(referrer)) {
                params.put("referrer", referrer);
            }
            if (!StringUtils.isEmpty(referrer)) {
                params.put("flashVer", flashVer);
            }
            if (!StringUtils.isEmpty(referrer)) {
                params.put("pageUrl", pageUrl);
            }
            if (client.getServerHostPort() != null) {
                String server = client.getServerHostPort().getAddressRawStr();
                if (!StringUtils.isEmpty(referrer)) {
                    params.put("server", server);
                }
            }
        }
        params.put("applicationName", application.getName());
        params.put("instanceName", instance.getName());
        params.put("vhost", host.getName());
        params.put("streamName", streamName);
        params.put("startTime", timeFormat.format(now));
        params.put("date", dateFormat.format(now));
        params.put("event", event);

        if (StringUtils.isEmpty(serverUrl)) {
            logger.error("Hey dude! can you please set up custom property named 'login_utl' in server wowza set up");
            return;
        }
        String json = generateJSON(params);
        if (StringUtils.isEmpty(json)) {
            return;
        }
        try {
            URL url = new URL(serverUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            writer.write(json);
            writer.close();
            int responseCode = connection.getResponseCode();
            logger.info("Request to " + serverUrl + ", response code: " + responseCode);
            writeLogFile(instance, serverUrl, responseCode);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static String generateJSON(Map<String, String> parameterMap) {
        JsonFactory jsonFactory = new JsonFactory();
        JsonGenerator generator = null;
        StringWriter out = new StringWriter();
        try {
            generator = jsonFactory.createGenerator(out);
            generator.writeStartObject();
            for(Map.Entry<String, String> entry : parameterMap.entrySet()) {
                generator.writeStringField(entry.getKey(), entry.getValue());
            }
            generator.writeEndObject();
        } catch (IOException e) {
            logger.error("Could not write response.", e);
            return null;
        } finally {
            if (generator != null) {
                try {
                    generator.close();
                } catch (IOException e) {
                    logger.error("Probably closing json generator", e);
                }
            }
        }
        return out.toString();
    }

    private void writeLogFile(IApplicationInstance instance, String serverUrl, int responseCode) {
        if(instance == null) {
            logger.error("Could not write log, instance not found.");
            return;
        }
        String path = instance.getStreamStoragePath();
        if(StringUtils.isEmpty(path)) {
            logger.error("Could not write log, storage path not found.");
            return;
        }
        Calendar now = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("YYYY/MM/dd hh:mm:ss a");
        String newLine = String.format("Request to %s with response code: %d at %s", serverUrl, responseCode, format.format(now.getTime()));

        List<String> lines = null;
        File parent = new File(path);
        File logFile = new File(parent, "request.log");
        try {
            if(!logFile.exists())  {
                lines = new ArrayList<>();
            } else {
                lines = FileUtils.readLines(logFile, "UTF-8");
            }
            lines.add(0, newLine);
            FileUtils.writeLines(logFile, lines);
        } catch (IOException e) {
            logger.error("Could not write log, reason: " + e.getMessage());
        }
    }
}
