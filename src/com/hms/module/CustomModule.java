package com.hms.module;

import com.wowza.wms.application.IApplicationInstance;
import com.wowza.wms.module.IModuleOnApp;
import com.wowza.wms.module.IModuleOnStream;
import com.wowza.wms.module.ModuleBase;
import com.wowza.wms.stream.IMediaStream;

public class CustomModule extends ModuleBase implements IModuleOnApp, IModuleOnStream  {

    private IApplicationInstance appInstance;
    private ActionNotify actionNotify;

    @Override
    public void onAppStart(IApplicationInstance appInstance) {
        this.appInstance = appInstance;
        this.actionNotify = new ActionNotify();
    }

    @Override
    public void onAppStop(IApplicationInstance iApplicationInstance) {
        this.actionNotify = null;
    }

    @Override
    public void onStreamCreate(IMediaStream mediaStream) {
        mediaStream.addClientListener(actionNotify);
    }

    @Override
    public void onStreamDestroy(IMediaStream mediaStream) {
        mediaStream.removeClientListener(actionNotify);
    }
}
